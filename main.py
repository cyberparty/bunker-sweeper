import discord
from glob import glob
import traceback
import sys
from cogs.util.bot_wrapper import BotWrapper

client = discord.Client()
bot = BotWrapper()

cogs = []
for cog in glob("./cogs/*.py"):
    file = cog.replace('\\', '/').split('/')[-1]
    cogname = 'cogs.' + file[:-3]
    cogs.append(cogname)
    
@bot.event
async def on_ready():
    print("Startup.\n")
    print("Name:", bot.user.name)
    print("ID:", bot.user.id)

if __name__ == "__main__":
    for cog in cogs:
        try:
            bot.load_extension(cog)
            print("Loaded " + cog + " OK")
        except Exception as e:
            print("Error loading " + cog + ".", file=sys.stderr)
            traceback.print_exc()

    bot.run_token()
