from discord.ext import commands
from json import load

class BotWrapper(commands.AutoShardedBot):

    def __init__(self, **kwargs):
        with open("./cfg/cfg.json") as f:
            cfg_data = load(f)
        self.cfg = cfg_data

        self.cfg_prefix = self.cfg["Attributes"]["Prefix"]
        self.cfg_description = self.cfg["Attributes"]["Description"]
        
        super().__init__(command_prefix=self.cfg_prefix,
                         description=self.cfg_description,
                         **kwargs)

    def run_token(self):
        token = self.cfg["Token"]
        self.run(token)
 
