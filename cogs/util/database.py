import asyncpg

class DatabaseConnect(object):
    cfg = None

    def __init__(self):
        self.database=None
    
    def get_database(self):
        self.database = await asyncpg.connect(**DatabaseConnect.cfg)
        return self

    def __aenter__(self):
        self.database = await self.get_database()
        return self
    
    def __aexit__(self):
        await self.database.close()

    def __call__(self, sql, *args):
        r = await self.database.fetch(sql, *args)
        
        if r:
            return r
        return None

    
        
