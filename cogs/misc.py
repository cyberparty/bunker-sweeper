from discord import File
from discord.ext import commands
from cogs.util.bot_wrapper import BotWrapper

class Misc(commands.Cog):

    def __init__(self, bot:BotWrapper):
        self.bot = bot
        
    @commands.command()
    async def ping(self, ctx):
        await ctx.send("Pong")
    
    @commands.Cog.listener()
    async def on_message(self, message):
        if "i wish" in message.content.lower():
            await message.channel.send("Wishing isn't very scientific, " + message.author.mention + "...", file=File("./img/misc_wish.png"))


def setup(bot:BotWrapper):
    bot.add_cog(Misc(bot))
        
